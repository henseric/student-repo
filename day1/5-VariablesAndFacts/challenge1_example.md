We will now create variables to be used in a playbook based on the earlier sections in this exercise.  

- Create a group variable for the arista group
  - group_var: arista

```bash
[student@ansible-1 ~]$ cd ~/student-repo/playbooks
[student@ansible-1 playbooks]$ mkdir group_vars
[student@ansible-1 playbooks]$ vi ./group_vars/arista
---
group_var: 'arista'
...

student@ansible-1 playbooks]$ cat ./group_vars/arista
---
group_var: 'arista'
...
student@ansible-1 playbooks]$
```


- Create a host variable for hosts rtr2 and rtr4
  - host_var: { rtr2 / rtr4 }

```bash
[student@ansible-1 playbooks]$ mkdir host_vars
[student@ansible-1 playbooks]$ vi ./host_vars/rtr2
---
host_var: rtr2
...

[student@ansible-1 playbooks]$ vi ./host_vars/rtr4
---
host_var: rtr4
...
```

Create the playbook to print out the variables for the hosts:

```yaml

---
- name: Print out variables for routers
  hosts: arista
  gather_facts: false

  tasks:
    - name: display group variable
      debug:
        msg: "The group variable is: {{ group_var }}"

    - name: display host variable
      debug:
        msg: "The host varialbe is:{{ host_vars }}"

...

```

 
Run the playbook and validate that it prints the correct variables.

