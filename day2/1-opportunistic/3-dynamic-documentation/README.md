## Return to Day 2 Exercises
* [Day 2 ](../../README.md)

# Exercise 3 - Dynamic-Documentation

  [Table of Contents](#table-of-contents)
  - [Step 1 - Job-templates](#step-1---job-templates)
  - [Step 2 - Run the Network-Config](#step-2-run-the-network-config)
  - [Step 3 - Run the Network-Compliance-Dashboard](#step-3-run-the-network-compliance-dashboard)
  - [Step 4 - Display the Dashboard](#step-4-display-the-dashboard)
  - [Step 5 - Review the files for the Dashboard](#step-5-review-the-files-for-the-dashboard)
  - [Step 6 - Change the Variables](#step-6-change-the-variables)
  - [Step 7 - Fix the role](#step-7-fix-the-role)
  - [Step 8 - Run again](#step-8-run-again)
  - [Step 9 - Optional Challenge](#step-9-optional-challenge)

## Solution
If needed.
[solution](solution/)

## Objective

Create a web based compliance dashboard and update with Ansible. 

## Overview

In this exercise the you will utilize and modify a `jinja2` template to organize compliance focused data collected with network `facts` using ansible and highlight the compliance violations in red. 
For this exercise we will also utilize a role to install a podman container with a nginx web service to view our dashboard with the lab pod's public IP Address on tcp port 8080. As an optional bonus this job-template can be ran from a job scheduler to periodically update the dashboard.   

### Step 1 - Job-templates
1. Create two new job templates with the following parameters. This step ommits the screenshot since job-templates were covered in the previous exercise.

#### Caution with selecting playbooks
`Caution` when selecting the playbooks for your job-templates from the drop down, please take care to `not` select the playbooks in the solution folder! 

#### Caution executions environments
**Sometimes the execution environment is `Validated Network` or `network workshop execution environment`**
Please use caution in the subsequent steps and exercises to choose the correct execution environments!

#### Job-template Parameters:
1.  Create the `Network-Config` Job Template
* Name: **Network-Config**
* Inventory: **Workshop Inventory**
* Project: **Student Project**
* Playbook: **day2/1-opportunistic/3-dynamic-documentation/config.yml**
* Credentials: **Machine / Workshop Credential**
* Execution Environment: **Validated Network**      
  
1.  Create the `Network-Compliance-Dashboard` Job Template
* Name: **Network-Compliance-Dashboard**
* Inventory: **Workshop Inventory**
* Project: **Student Project**
* Playbook: **day2/1-opportunistic/3-dynamic-documentation/network_report.yml**
* Credentials: **Machine / Workshop Credential**
* Execution Environment: **network workshop execution environment**


### Step 2 - Run the Network-Config
This job-template will run a playbook to configure some of the  items under the scope of compliance.

### Step 3 - Run the Network-Compliance-Dashboard
Copy the IP address for the dashboard in the job output.

### Step 4 - Display the Dashboard
Open a tab from your own browser and paste the url from the previous step. You will see the dashboard highlighted in red for elements that are out of compliance. Note, rtr3 is not yet diplayed. Take a few minutes to explore the dashboard and associated dropdown menus.

![Dash](../../images/dash1.png)

### Step 5 - Review the files for the Dashboard

~~~
 $ cat ~/student-repo/day2/1-opportunistic/3-dynamic-documentation/roles/build_report_container/templates/report.j2
~~~

 Note, that the `mark` tag is used to highlight elements that are not in compliance. Notice how the mark is used in the following example.

~~~
<td class="sub_net_info">{% if hostvars[network_switch]['ansible_net_version'] != desired_ios_version and hostvars[network_switch]['ansible_network_os'] == 'ios' %}<mark>{{hostvars[network_switch]['ansible_net_version']}}</mark>{% elif hostvars[network_switch]['ansible_net_version'] != desired_eos_version and hostvars[network_switch]['ansible_network_os'] == 'eos' %}<mark>{{hostvars[network_switch]['ansible_net_version']}}</mark>{% elif hostvars[network_switch]['ansible_net_version'] != desired_junos_version and hostvars[network_switch]['ansible_network_os'] == 'junos' %}<mark>{{hostvars[network_switch]['ansible_net_version']}}</mark>{% else %}{{hostvars[network_switch]['ansible_net_version']}}{% endif %}</td>
~~~     
         
 The vars/main.yml file includes the `desired_ios_version` and other variables used for the `jinja2` configuration.
- The file is located here: `day2/1-opportunistic/3-dynamic-documentation/roles/build_report_container/vars/main.yml`

 ~~~
desired_ios_version: "17.03.06"
desired_eos_version: "4.27.1F-cloud"
desired_junos_version: "22.3R2.11"
desired_snmp: "student1"
desired_logging_host: "192.168.0.254"
~~~

This is the current contents of the network_report.yml playbook.
`day2/1-opportunistic/3-dynamic-documentation/network_report.yml`
~~~
---
- name: Compliance Dashboard
  hosts: network
  gather_facts: false
  
  tasks:
    - name: Load read interfaces role
      ansible.builtin.include_role:
        name: "../roles/facts"

- name: Build report with facts
  hosts: ansible-1
  become: true
  gather_facts: false
  vars_files:
  - vars/vars.yml

  tasks:

    - name: Build a report
      ansible.builtin.include_role:
        name: "../roles/build_report_container"
~~~

### Step 6 - Change the Variables
The variables are located in the folloing file:
~~~
/day2/1-opportunistic/3-dynamic-documentation/roles/build_report_container/vars/main.yml
~~~

1.  Replace the section of the vars.yml with the following

~~~
desired_ios_version: "17.03.06"
desired_eos_version: "4.27.3F-cloud"
desired_junos_version: "22.3R2.12"
desired_snmp: "student1"
desired_logging_host: "192.168.0.1"
~~~

### Step 7 - Fix the role
1. Configure the role `facts/tasks/junipernetworks.junos.junos.yml` to gather facts for the Juniper router (rtr3)
- see the TODO section below.
~~~
---
- name: Gather juniper junos facts
  # TODO: gather all network resources for juniper
  debug:
   msg: remove me!
~~~

> HINT: take a look at the ios.yml for an example or if you're sort of lazy - that's ok too because you're becoming a developer - feel free look in the solution folder.

2. Don't forget to save the files and update to the gitlab repo from the terminal: \
Disregard if you already performed the Day 1 getting started steps here: 
[Day 1 - Getting Started](https://gitlab.com/taruch/student-repo/-/blob/main/day1/1-GettingStarted/README.md)

> NOTE: To avoid adding password/token for the terminal or VSCode enter the folling command in the terminal:
  **Disregard if completed during the Day 1 exercises**
  ~~~
    git remote set-url origin https://<username>:<token>@gitlab.com/<username>/student-repo.git 
  ~~~

### Commit and Push
- From the terminal:
~~~
git add --all
git commit -m "vars"
git push
~~~

- or you can use the VSCode
![Dash](../../images/save_commit.png)

### Step 8 - Run again
Run the `Network-Compliance-Dashboard` job-template and reload the dashboard tab on your browser.
* Note, at this point no red highlights are displayed because all configurations are compliant.
- note the url-ip address for the dashboard in the job output.

![Dash](../../images/dash2.png)

### Step 9 - Optional Challenge
Create a schedule to run periodically the for the Network-Compliance-Dashboard job-template. No hints available. Good luck!!!

## Next Exercise
* [Network Compliance ](../../2-systematic/4-network-compliance/README.md)

[Click Here to return to the Ansible Network Automation Workshop](../README.md)

