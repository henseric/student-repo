# Day 2 Exercises
# Ansible Network Automation Workshop 201


**This is documentation for Ansible Automation Platform 2**

The Ansible Network Automation workshop 201 Day 2 is a comprehensive intermediate guide to automating popular network data center devices from Arista, Cisco and Juniper via Ansible AAP. You’ll explore many network automation use cases and learn how to apply automation using the Ansible automation controller. You’ll put it all together by completing a fun and challenging capstone project to complete the day of exercises.

## Presentation

Want the Presentation Deck?  Its right here:
- [Ansible Network Automation Workshop 201 Day 2 Deck](https://gitlab.com/redhatautomation/ansible-network-automation-workshop-201/-/blob/main/day2/lab_guide/Network_Automation_Workshop_Deck_201_Day2.pdf) PDF


## Ansible Network Automation Exercises
### Opportunistic
* [Exercise 1 - Controller as Code](./1-opportunistic/1-controller-as-code/README.md)
* [Exercise 2 - Backups and Restore](./1-opportunistic/2-backup-and-restore/README.md)
* [Exercise 3 - Dynamic Documentation](./1-opportunistic/3-dynamic-documentation/README.md)
### Systematic
* [Exercise 4 - Network Compliance](./2-systematic/4-network-compliance/README.md)
* [Exercise 5 - Scoped Config Management](./2-systematic/5-scoped-config-management/README.md)
### Institutionalized (Capstone)
* [Exercise 6 - Automated NetOps and Operation Validation](./3-institutionalized/6-automated-netops-validation/README.md)

## Network Diagram

![Red Hat Ansible Automation](https://github.com/ansible/workshops/blob/devel/images/ansible_network_diagram.png?raw=true)

---
![Red Hat Ansible Automation](https://github.com/ansible/workshops/blob/devel/images/rh-ansible-automation-platform.png?raw=true)


## Return Home
* [ The Main README.md](../README.md)
