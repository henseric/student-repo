## Return to Day 2 Exercises
* [Day 2 ](../../README.md)



# Exercise 4 - Network Compliance

[Table of Contents](#table-of-contents)
  - [Step 1 - Job-templates](#step-1---job-templates)
  - [Step 2 - Create a Workflow](#step-2---create-a-workflow)
  - [Step 3 - Create a Survey](#step-3---create-a-survey)
  - [Step 4 - Run the Workflow](#step-4---run-the-workflow)
  - [Step 5 - Fix the issue](#step-5---fix-the-issue)
  - [Step 6 - Rerun the Workflow](#step-6---rerun-the-workflow)
  - [Step 7 - Verify the completed workflow](#step-7---verify-the-completed-workflow)
  - [Optional Challenge](#optional-challenge)
  
## Solution
If needed.
[solution](solution/)

## Objective

Create a Workflow with a `self service survey` to select groups and roles to run compliance checks against. 

## Overview

In this exercise the you will utilize roles, playbooks, job-templates, surveys, and create a workflow to manage compliance checks for a multi-vendor network. 

### Step 1 - Job-templates
1. Create two new job templates to match the following screen shots. To save time, feel free to use the copy function after completing the Network-Check-Compliance job-template to create Network-Run-Compliance.

- Name: **Network-Check-Compliance**
- Job Type: **Check**
- Inventory: **Workshop Inventory**
- Project: **Student Project**
- Execution **Environment: network workshop execution environment**
- Playbook: **day2/2-systematic/4-network-compliance/compliance.yml**
- Credentials: **Machine / Workshop Credential**

Copy the Network-Check-Compliance job template and update the Name and Job Type.
- Name: **Network-Run-Compliance**
- Job Type: **Run**


#### Job-template Parameters:
![Network-Check-Compliance](../../images/check.png)
Check mode will run the playbook to list the before state and the necessary remediation commands but not actually replace the configuration on the targetted devices.

![Network-Run-Compliance](../../images/run.png)
Run mode will replace the needed configurations.

### Step 2 - Create a Workflow
By using a `workflow` we can orchestrate running a compliance check in `check mode` first, such that we can glean what configurations are out-of-compliance. Subsequently, change approvals and downtime are needed prior to making changes, hense we will add an approval node to our workflow. This way we can first review the needed changes and approve the workflow to continue forward with the compliance job-template in `run mode` when appropriate. Lastly, we will provide self service capabilities with a survey to allow the network operators to select which groups of devices and compliance roles to run for the workflow.

1. Create the Workflow template with the following parameters:
![Network-Compliance-Workflow](../../images/compliance_workflow.png)

2. Start the visialization with the following Nodes
- Link each node together with "green" pass conditions.

- Job-Template
![Network-Compliance-Check](../../images/network-check-compliance.png)

- Approval (makes sure to select approval node)
![Network-Compliance-Approval](../../images/network-change-approval.png)

- Job-Template
![Network-Compliance-Run](../../images/network_run_compliance.png)


The workflow should look like the following:
![Completed-Compliance](../../images/completed_net_compliance.png)
save the workflow template

### Step 3 - Create a Survey
Self Service

1. Locate the Network-Compliance-Workflow again from the templates and select survey.
![Select Survey](../../images/selectsurvey.png)

2. Create a survey question to determine the targetted device groups.
![Select Group](../../images/survey_groups.png)

3. Create a survey question to select the desired compliance roles.
![Select Role](../../images/compliance_tag.png)

4. Enable the survey.
![Enable Survey](../../images/enable_survey.png)


### Step 4 - Run the Workflow
Locate and Launch the Network-Compliance-Workflow. In the survey, select all groups and all compliane roles.

Oh dang! What in the actual.... It didn't work!!! This is some... 
~~~
ERROR! no module/action detected in task.

The error appears to be in '/runner/project/day2/2-systematic/4-network-compliance/roles/logging/tasks/ios.yml': line 2, column 3, but may
be elsewhere in the file depending on the exact syntax problem.
The offending line appears to be:
---
- name: Check/Apply IOSXE logging configuration
  ^ here
~~~
                
### Step 5 - Fix the issue
Hint, you can look at some of the other .yml files in the logging/task for ideas. In fact, this is a good opportunity to checkout the `compliance.yml` playbook and the roles in the `4-network-compliance folder`. If needed, feel free to checkout the solutions folder.

These are the files of note. In particular the roles tasks and vars files. The vars/main.yml files provide the approved compliance configurations for each role.
~~~
ansible-network-automation-workshop-201/day2/2-systematic/4-network-compliance
├── compliance.yml
├── README.md
├── roles
│   ├── logging
│   │   ├── defaults
│   │   │   └── main.yml
│   │   ├── files
│   │   ├── handlers
│   │   │   └── main.yml
│   │   ├── meta
│   │   │   └── main.yml
│   │   ├── README.md
│   │   ├── tasks
│   │   │   ├── eos.yml
│   │   │   ├── ios.yml
│   │   │   ├── junos.yml
│   │   │   └── main.yml
│   │   ├── templates
│   │   ├── tests
│   │   │   ├── inventory
│   │   │   └── test.yml
│   │   └── vars
│   │       ├── logging
│   │       └── main.yml
│   ├── ntp
│   │   ├── defaults
│   │   │   └── main.yml
│   │   ├── files
│   │   ├── handlers
│   │   │   └── main.yml
│   │   ├── meta
│   │   │   └── main.yml
│   │   ├── README.md
│   │   ├── tasks
│   │   │   ├── eos.yml
│   │   │   ├── ios.yml
│   │   │   ├── junos.yml
│   │   │   └── main.yml
│   │   ├── templates
│   │   ├── tests
│   │   │   ├── inventory
│   │   │   └── test.yml
│   │   └── vars
│   │       ├── logging
│   │       └── main.yml
│   └── snmp
│       ├── defaults
│       │   └── main.yml
│       ├── files
│       ├── handlers
│       │   └── main.yml
│       ├── meta
│       │   └── main.yml
│       ├── README.md
│       ├── tasks
│       │   ├── eos.yml
│       │   ├── ios.yml
│       │   ├── junos.yml
│       │   └── main.yml
│       ├── templates
│       ├── tests
│       │   ├── inventory
│       │   └── test.yml
│       └── vars
│           ├── logging
│           └── main.yml
~~~

Please note, You must save, commit the file in the VSCode IDE and "sync" push to gitlab after fixing the roles/logging/tasks/ios.yml locally.
![Save](../../images/save_commit.png)

or update from the terminal
~~~
git add --all
git commit -m "workflow"
git push
~~~

### Step 6 - Rerun the Workflow
This time the workflow should run the Network-Compliance-Check node but pause at the approval node. 
1. Review the Network-Compliance-Check job and then approve the approval node.

![approval](../../images/approval_stop.png)

![approval](../../images/approve_button.png)


### Step 7 - Verify the completed workflow

1. The completed workflow should look like the following
![approval](../../images/compliance_success.png)

2. Review the Network-Compliance-Run job

3. Optional, Review the router configs.
For example:
~~~
ssh rtr1

sh run | s ntp
~~~

#### Note if you receive an error with SSH 
If the Cisco router prompts for a password then ctl C and exit. This is a no mutual signature supported error.
- type the command at the Linux shell
~~~
update-crypto-policies --set LEGACY
~~~
No need to reload

### Optional Challenge 
Add to the workflow survey another dropdown role choice called 'stig'. Please note this option can only succeed, if selecting only the Cisco group and only stig for the role in the survey.


Congratulations your devices are compliant!!!! or at least for now ;-)

## Next Exercise
* [ 5-scoped-config-management](../../2-systematic/5-scoped-config-management/README.md)

[Click Here to return to the Ansible Network Automation Workshop](../README.md)



